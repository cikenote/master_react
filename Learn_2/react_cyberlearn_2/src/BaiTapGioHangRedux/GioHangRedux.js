import React, { Component } from "react";

// sử dụng thư viện connect để lấy dữ liệu từ store về
import { connect } from "react-redux";

class GioHangRedux extends Component {
  render() {
    console.log(this.props.gioHang);

    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade"
          id="modelId"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div
            className="modal-dialog"
            style={{ minWidth: 1000 }}
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title text-center">Giỏ hàng</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Mã SP</th>
                      <th>Hình Ảnh</th>
                      <th>Tên SP</th>
                      <th>Số lượng</th>
                      <th>Giá</th>
                      <th>Thành tiền</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.gioHang.map((spGH, index) => {
                      return (
                        <tr key={index}>
                          <td>{spGH.maSP}</td>
                          <td>
                            <img
                              src={spGH.hinhAnh}
                              alt={spGH.hinhAnh}
                              width={50}
                              height={50}
                            />
                          </td>
                          <td>{spGH.tenSP}</td>
                          <td>{spGH.gia.toLocaleString()}</td>
                          <td>{spGH.soLuong}</td>
                          <td>{(spGH.gia * spGH.soLuong).toLocaleString()}</td>
                          <td
                            className="btn btn-danger"
                            onClick={() => {
                              this.props.xoaGioHang(spGH.maSP);
                            }}
                          >
                            Xóa
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// Hàm lấy state redux biến đổi thành props của component
const mapStateToProps = (state) => {
  // state là state tổng của ứng dụng chứa các state con (rootReducer)
  return {
    gioHang: state.stateGioHang.gioHang,
  };
};

// Đưa dữ liệu lên reducer
const mapDispatchToProps = (dispatch) => {
  return {
    xoaGioHang: (maSP) => {
      // Tạo action
      let action = {
        type: "XOA_GIO_HANG",
        maSP,
      };
      // dùng phương thức dispatch do redux cung cấp để gửi dữ liệu đi
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(GioHangRedux);
