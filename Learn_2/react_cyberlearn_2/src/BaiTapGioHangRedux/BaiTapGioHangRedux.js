import React, { Component } from "react";
import GioHangRedux from "./GioHangRedux";
import ProductListRedux from "./ProductListRedux";
// import thư viện connect kết nối react compoent và redux store
import {connect} from "react-redux";

class BaiTapGioHangRedux extends Component {

  renderSoLuong = () => {
    return this.props.gioHang.reduce((tongSoLuong, spGH, index) => {
      return (tongSoLuong += spGH.soLuong)
    }, 0).toLocaleString();
  }
  render() {
    return (
      <div className="container">
        <h3 className="text-center">Danh sách sản phẩm</h3>
        <div className="text-right">
          <span
            data-toggle="modal"
            data-target="#modelId"
            style={{ cursor: "pointer" }}
          >
            <i class="fa fa-shopping-cart mr-5" aria-hidden="true">
              ({this.renderSoLuong()})Giỏ hàng
            </i>
          </span>
        </div>
        <ProductListRedux />
        <GioHangRedux />
      </div>
    );
  }
}

// viết hàm lấy giá trị state từ redux store về biến thành props components
const mapStateToProps = (state) => {
  return {
    gioHang: state.stateGioHang.gioHang,
  }
}

export default connect(mapStateToProps)(BaiTapGioHangRedux)
