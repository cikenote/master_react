import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    return (

      <div class="card text-left" style={{ width: "200px" }}>
        <img class="card-img-top" src={this.props.dataProductItem.image} alt={this.props.dataProductItem.alias} />
        <div class="card-body">
          <h4 class="card-title">{this.props.dataProductItem.name}</h4>
          <h5 class="card-text">{this.props.dataProductItem.price}</h5>
          <p class="card-text">{this.props.dataProductItem.shortDescription}</p>
          <button className="w3-btn w3-black text-white">
            Add to cart <i class="fa fa-shopping-cart"></i>
          </button>
        </div>
      </div>
    );
  }
}
