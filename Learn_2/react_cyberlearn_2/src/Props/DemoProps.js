import React, { Component } from "react";
import Parent from "./Parent";
import DanhSachSanPham from "./DanhSachSanPham";

export default class DemoProps extends Component {
  refParent = React.createRef();

  changeTitle = () => {
    this.refParent.current.changeTitle();
  };

  render() {
    return (
      <div>
        <button onClick={this.changeTitle}>Change Title</button>
        <Parent ref={this.refParent}>
          <DanhSachSanPham />
          <h3>Hello Cikenote</h3>
          <DanhSachSanPham />
          <DanhSachSanPham />
        </Parent>
      </div>
    );
  }
}

// Props: là thuộc tính nhận giá trị từ component cha vào => Props được truy xuất thông qua
// thuộc tính this.props.propsname
