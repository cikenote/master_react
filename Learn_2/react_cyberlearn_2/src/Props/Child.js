import React, { Component } from "react";

export default class Child extends Component {


    
  renderSize = () => {
    let {size} = this.props.productItem;
    return size.map((number, index) => {
      return <button key={index}>{number}</button>
    })
  }


  render() {
    let {src, name, price, des} = this.props.productItem;
    let {shoesSize} = this.props;
    return (
      <div>
        {/* <img style={{ width: 150 }} src={this.props.propSource} /> */}
        <div className="card text-left" style={{ width: 250 }}>
          <img className="card-img-top"  src={src} alt="Ảnh nè" />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{des}</p>
            <p className="card-text">{price}</p>
            {this.renderSize()}
            {shoesSize.map((number, index) => {
              return <button key={index} className="btn btn-success">{number}</button>
            })}
          </div>
        </div>
      </div>
    );
  }
}
