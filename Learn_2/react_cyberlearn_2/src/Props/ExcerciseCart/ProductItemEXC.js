import React, { Component } from "react";

export default class ProductItemEXC extends Component {
  render() {
    let { sanPhamProps } = this.props;
    return (
      <div>
        <div className="card text-left">
          <img
            className="card-img-top"
            style={{ width: 300, height: 300 }}
            src={sanPhamProps.hinhAnh}
            alt={sanPhamProps.tenSP}
          />
          <div className="card-body">
            <h4 className="card-title text-center">{sanPhamProps.tenSP}</h4>
            <p className="card-text text-center">{sanPhamProps.giaBan.toLocaleString()}</p>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.themGioHang(sanPhamProps);
              }}
            >
              Thêm giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}
