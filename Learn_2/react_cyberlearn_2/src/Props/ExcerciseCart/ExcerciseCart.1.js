import React, { Component } from "react";
import CartModal from "./CartModal";
import ProductListEXC from "./ProductListEXC";

export default class ExcerciseCart extends Component {
  state = {
    gioHang: [],
  };

  themGioHang = (sanPham) => {
    // console.log(sanPham);
    let spGioHang = {
      maSP: sanPham.maSP,
      tenSP: sanPham.tenSP,
      donGia: sanPham.giaBan,
      soLuong: 1,
      hinhAnh: sanPham.hinhAnh,
    };

    // Tìm xem sản phẩm đã có trong giỏ hàng chưa
    let index = this.state.gioHang.findIndex(
      (spGH) => spGH.maSP === spGioHang.maSP
    );
    if (index !== -1) {
      // Tìm thấy sản phầm được click chứa trong giỏ hàng => tăng số lương
      this.state.gioHang[index].soLuong += 1;
    } else {
      // Không tìm thấy trong giỏ hàng chứa sản phẩm đó => thêm sản phẩm vào giỏ
      this.state.gioHang.push(spGioHang);
    }

    // push vào state.gioHang
    // let gioHangCapNhat = [...this.state.gioHang, spGioHang];
    this.setState({
      gioHang: this.state.gioHang,
    });
  };

  xoaGioHang = (maSP) => {
    // thực hiện tính năng xóa giỏ hàng
    console.log(maSP);
    // this.setState....
    let index = this.state.gioHang.findIndex(
      (spGioHang) => spGioHang.maSP == maSP
    );
    if (index !== -1) {
      this.state.gioHang.splice(index, 1);
    }
    this.setState({
      gioHang: this.state.gioHang,
    });
  };

  tinhTongSoLuong = () => {
    // dùng For
    // let tongSoLuong = 0;
    // for(let i = 0; i < this.state.gioHang.length; i++) {
    //   let spGioHang = this.state.gioHang[i];
    //   tongSoLuong += spGioHang.soLuong;
    // }
    // return tongSoLuong;

    // Reduce
    // const sum = numbers.reduce((accumulator, currentValue) => {
    //   return accumulator + currentValue;
    // }, 0);
    // Giá trị 0 trong ví dụ là giá trị khởi đầu của biến tích lũy (accumulator)
    // accumulator: Giá trị tích lũy từ các lần gọi trước
    // currentValue: Giá trị hiện tại của phần tử trong mảng
    // index: Chỉ mục của phần tử hiện tại
    // array: Mảng đang được gọi reduce
    return this.state.gioHang.reduce((tongSoLuong, spGioHang, index) => {
      return (tongSoLuong += spGioHang.soLuong);
    }, 0);
  };

  render() {
    return (
      <div className="container-fluid">
        <h3 className="text-center">Bài tập giỏ hàng</h3>
        <div className="text-right">
          <span
            data-toggle="modal"
            data-target="#modelId"
            style={{ cursor: "pointer" }}
          >
            <i class="fa fa-shopping-cart mr-5" aria-hidden="true">
              ({this.tinhTongSoLuong()})Giỏ hàng
            </i>
          </span>
        </div>
        <CartModal xoaGioHang={this.xoaGioHang} gioHang={this.state.gioHang} />
        <ProductListEXC themGioHang={this.themGioHang} />
      </div>
    );
  }
}
