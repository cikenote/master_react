import React, { Component } from "react";
import ProductItem from "./ProductItem"
export default class ProductList extends Component {

    renderProductItem = () => {
        // this.props.productData lấy từ ExcerciseCarStoreComponent
        return this.props.productData.map((product,index) => {
            return (
                <div className="col-3" key={index}>
                    <ProductItem item={product} xemChiTiet={this.props.xemChiTiet}/>
                </div>
            )
        })
    }
  render() {
    return <div className="container">
        <div className="row">
        {this.renderProductItem()}
        </div>
    </div>;
  }
}
