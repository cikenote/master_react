import React from "react";

export default function ChildFunctional(props) {

    let {src, name, price, des} = props.productItem;
  return (
    <div>
      <div className="card text-left" style={{ width: 250 }}>
        <img
          className="card-img-top"
          src={src}
          alt="Ảnh nè"
        />
        <div className="card-body">
          <h4 className="card-title">{name}</h4>
          <p className="card-text">{des}</p>
          <p className="card-text">{price}</p>
        </div>
      </div>
    </div>
  );
}
