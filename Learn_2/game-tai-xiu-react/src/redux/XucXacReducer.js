

const stateDefault = {
    taiXiu: true, // true là tài từ(>12),false là xỉu (3-11);
    mangXucXac: [
        {ma: 6, hinhAnh:"./img/game_xuc_xac/6.png"},
        {ma: 6, hinhAnh:"./img/game_xuc_xac/6.png"},
        {ma: 6, hinhAnh:"./img/game_xuc_xac/6.png"},
    ],
    soBanThang: 0,
    tongSoBanChoi: 0,
}

const XucXacReducer = (state = stateDefault,action) => {
    switch(action.type) {
        case "DAT_CUOC":
            state.taiXiu = action.taiXiu;
            return {...state};
            // break;
        case "PLAY_GAME":
            // Bước 1 xử lý random xúc xắc
            let mangXucXacNgauNghien = [];
            for(let i = 0; i < 3; i++) {
                // Mối lần lặp random ra số ngẫu nhiên từ 1 đến 6
                let soNgauNghien = Math.floor(Math.random() * 6) + 1;
                // Tạo ra 1 đối tượng xúc xắc từ số ngẫu nhiên
                let xucXacNgauNhien = {ma: soNgauNghien, hinhAnh: `./img/game_xuc_xac/${soNgauNghien}.png`};
                // push vào mảng xucXacNgauNhien
                mangXucXacNgauNghien.push(xucXacNgauNhien);
            }
            // Gán state mangXucXac = mangXucXacNgauNhien
            state.mangXucXac = mangXucXacNgauNghien;

            // Xử lý tăng bàn chơi
            state.tongSoBanChoi += 1;

            // Xử lý số bàn thắng
            let tongSoDiem = mangXucXacNgauNghien.reduce((tongDiem, xucXac, index) => {
                return tongDiem += xucXac.ma;
            }, 0)
            // Xét điều kiện để người dùng win game
            if((tongSoDiem > 11 && state.taiXiu == true) || (tongSoDiem <= 11 && state.taiXiu == false)) {
                state.soBanThang += 1;
            }
            return {...state}
        default: return {...state};
    }
}

export default XucXacReducer;