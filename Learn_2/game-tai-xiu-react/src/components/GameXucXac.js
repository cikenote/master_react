import React, { Component } from "react";
import "../css/GameXucXac.css";
import XucXac from "./XucXac";
import ThongTinTroChoi from "./ThongTinTroChoi";
import { connect } from "react-redux";

class GameXucXac extends Component {
  render() {
    return (
      <div className="game">
        <div className="title-game text-center mt-5 display-4">
          TÀI XỈU ONLINE
        </div>
        <div className="row text-center">
          <div className="col-4">
            <button
              onClick={() => {
                this.props.datCuoc(true);
              }}
              className="btnGame"
            >
              TÀI
            </button>
          </div>
          <div className="col-4">
            <XucXac />
          </div>
          <div className="col-4">
            <button
              onClick={() => {
                this.props.datCuoc(false);
              }}
              className="btnGame"
            >
              XỈU
            </button>
          </div>
        </div>
        <div className="thongTinTroChoi text-center">
          <ThongTinTroChoi />
          <button onClick={() => {this.props.playGame()}} className="btn btn-success mt-5 display-4 p-2">
            PLAY GAME
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (taiXiu) => {
      // tạo action tài xỉu
      let action = {
        type: "DAT_CUOC",
        taiXiu,
      };
      // gửi lên reducer
      dispatch(action);
    },
    playGame: () => {
      let action = {
        type: "PLAY_GAME",
      }
      // gửi dữ liệu type PLAY_GAME lên reducer
      dispatch(action);
    }
  };
};

export default connect(null,mapDispatchToProps)(GameXucXac);
