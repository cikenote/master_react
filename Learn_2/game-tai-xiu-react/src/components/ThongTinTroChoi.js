import React, { Component } from "react";
import {connect} from "react-redux";
class ThongTinTroChoi extends Component {
  render() {
    return (
      <div>
        <div className="display-4">
          BẠN CHỌN: <span className="text-danger">{this.props.taiXiu ? "TÀI" : "XỈU"}</span>
        </div>
        <div className="display-4">
          BÀN THẮNG: <span className="text-success">{this.props.soBanThang}</span>
        </div>
        <div className="display-4">
          TỔNG SỐ BÀN CHƠI: <span className="text-primary">{this.props.tongSoBanChoi}</span>
        </div>
      </div>
    );
  }
}

// Hàm lấy dữ liệu từ store về
const mapStateToProps = (state) => {
    return {
        soBanThang: state.XucXacReducer.soBanThang,
        tongSoBanChoi: state.XucXacReducer.tongSoBanChoi,
        taiXiu: state.XucXacReducer.taiXiu,
    }
}

export default connect(mapStateToProps)(ThongTinTroChoi)
