import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { ToDoListDarkTheme } from "../themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../themes/ToDoListPrimaryTheme";
import { Container } from "../components/Container";
import { Dropdown } from "../components/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../components/Heading";
import { Label, TextField, Input } from "../components/TextField";
import { Button } from "../components/Button";
import { Table, Tbody, Td, Th, Thead, Tr } from "../components/Table";
import { connect } from "react-redux";
import { add_task, change_theme } from "../redux/type/ToDoListTypes";
import {
  addTaskAction,
  changeThemAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  updateTask,
} from "../redux/actions/DoToListActions";
import { arrTheme } from "../themes/ThemeManager";
class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr style={{ height: 100 }} key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabled: false,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                }}
                className="ml-1"
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskComplete = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr style={{ height: 100 }} key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  // Viết hàm render theme, import ThemeManger
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  // Lifecycle bảng 16 nhận vào props mới được thực thi trước khi render
  // componentWillReceiveProps(newProps) {
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   })
  // }

  // Lifecycle tĩnh không truy xuất được con trỏ this
  // static  getDerivedStateFromProps(newProps, currentState) {
  //   // newPropS: là props mới, props cũ là this.props (không truy xuất được)
  //   // currentState: ứng với state hiện tại - this.state

  //   // hoặc trả về state mới (this.state)
  //   let newState = {...currentState, taskName: newProps.taskEdit.taskName}
  //   return newState;

  //   // trả về null thì state giữ nguyên
  //   // return null;
  // }

  // getDerivedStateFromProps khác với componentWillReceiveProps

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50 mt-5">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              // Dispatch value lên reducer
              this.props.dispatch(changeThemAction(value)); // value là themeId
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading2>To Do List</Heading2>
          <TextField
            value={this.state.taskName}
            name="taskName"
            onChange={(e) => {
              this.setState({ taskName: e.target.value });
            }} //
            label="Task Name"
            className="w-50"
          ></TextField>
          <Button
            className="ml-2"
            onClick={() => {
              // Lấy thông tin người dùng nhập vào từ input
              let { taskName } = this.state;

              // Tạo ra 1 task object
              let newTask = {
                id: Date.now(), // trả về dưới dạng số ngẫu nhiên
                taskName: taskName,
                done: false,
              };
              // Đưa task object lên redux thông qua phương thức dispatch
              this.props.dispatch(addTaskAction(newTask));
            }}
          >
            <i className="fa fa-plus"></i> Add Task
          </Button>
          {this.state.disabled ? (
            <Button
              disabled
              className="ml-2"
              onClick={() => {
                this.props.dispatch(updateTask(this.state.taskName));
              }}
            >
              <i className="fa fa-upload"></i> Update Task
            </Button>
          ) : (
            <Button
              className="ml-2"
              onClick={() => {
                let {taskName} = this.state;
                this.setState(
                  {
                    disabled: true,
                    taskName: "",
                  },
                  () => {
                    this.props.dispatch(updateTask(taskName));
                  }
                );
              }}
            >
              <i className="fa fa-upload"></i> Update Task
            </Button>
          )}

          <hr />

          <Heading3>Task To Do</Heading3>
          <Table>{this.renderTaskToDo()}</Table>
          <Heading3>Task Complete</Heading3>
          <Table>{this.renderTaskComplete()}</Table>
        </Container>
      </ThemeProvider>
    );
  }

  // Lifecycle trả về props cũ và state cũ của component trước khi render
  // lifecycle này chạy sau hàm render
  // có điều kiện không thôi nó chạy từ render rồi lại về componentDidUpdate =>lặp hoài lặp mãi
  componentDidUpdate(prevProps, prevState) {
    // So sánh nếu như props trước đó (taskEdit trước mà khác taskEdit hiện thì mình mới setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);

// Vấn đề gặp phải trong lifecycle
// - Dữ liệu value input taslName được lấy từ props(state từ redux).
// - Mỗi lần người dùng nhập liệu cần binding giá trị vào state => setSate => lifecycle
// phương thức render chạy lại => input lại lấy dữ liệu từ redux mà không phải từ người dùng nhập vào

// + Khắc phực:
// - Xử lý trước khi hàm render được gọi ta sẽ can thiệp bằng lifecycle để lấy props từ redux => gắn
// thuộc tính this.state của component và value của input pphair binding từ state
