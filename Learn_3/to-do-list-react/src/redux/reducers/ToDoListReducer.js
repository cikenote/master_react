import { ToDoListDarkTheme } from "../../themes/ToDoListDarkTheme";
import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
  update_task,
} from "../type/ToDoListTypes";
import { arrTheme } from "../../themes/ThemeManager";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: "task-1", taskName: "Task 1", done: true },
    { id: "task-2", taskName: "Task 2", done: false },
    { id: "task-3", taskName: "Task 3", done: true },
    { id: "task-4", taskName: "Task 4", done: false },
  ],
  taskEdit: { id: "-1", taskName: "", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      if (action.newTask.taskName.trim() === "") {
        // kiểm tra rỗng
        alert("Task name is required!!!");
        return { ...state };
      }
      // kiểm tra tồn tại
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== -1) {
        alert("Task name alreadu exists!!!");
        return { ...state };
      }
      taskListUpdate.push(action.newTask);

      // xử lý xong thì gắn taskList mới vào taskList hiện tài
      state.taskList = taskListUpdate;
    }
    case change_theme: {
      // tìm theme dựa vào action.themId được chọn
      let theme = arrTheme.find((theme) => theme.id == action.themeId);
      if (theme) {
        // set lại theme cho state.themeToDoList
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case done_task: {
      // Click vào button check => dispatch lên action có taskId
      let taskListUpdate = [...state.taskList];
      // Từ taskId tìm ra task đó ở vị trí nào trong mảng tiến hành cập nhật lại thuộc tính
      // done = true. và cập nhật lại state của redux
      let index = taskListUpdate.findIndex((task) => task.id == action.taskId);

      if (index != -1) {
        taskListUpdate[index].done = true;
      }

      state.taskList = taskListUpdate;
      return { ...state };
    }
    case delete_task: {
      let taskListUpdate = [...state.taskList];
      // Gán lại giá trị cho mảng taskListUpdate bằng chính nó nhưng filter không có taskId đó
      taskListUpdate = taskListUpdate.filter(
        (task) => task.id !== action.taskId
      );
      return { ...state, taskList: taskListUpdate };
    }
    case edit_task: {
      return { ...state, taskEdit: action.task };
    }
    case update_task: {
      // Chỉnh sửa lại taskName của taskEdit
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };
      // tìm trong taskList cập nhật lại taskEdit người dùng update
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(
        task => task.id === state.taskEdit.id
      );
      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskList = taskListUpdate;
      state.taskEdit = {id: "-1", taslName:"", done: false};

      return { ...state };
    }
    default:
      return { ...state };
  }
};
