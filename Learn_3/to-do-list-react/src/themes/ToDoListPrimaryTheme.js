export const ToDoListPrimaryTheme = {
    bgColor: '#fff',
    color:'#993a40',
    borderButton: '1px solid #993a40',
    borderRadiusButton: 'none',
    hoverTextColor: '#fff',
    hoverBgColor:'#993a40',
    borderColor:'#993a40',
}