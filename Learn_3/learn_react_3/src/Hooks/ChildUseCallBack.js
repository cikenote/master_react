import React, { memo } from "react";

 function ChildUseCallBack(props) {
  let title = "Cikenote";
  let object = { id: 1, name: "Danh tiên xinh" };
  console.log("title", title);
  console.log("object", object);
  console.log("re-render")
  return (
    <div>
      {/* <small>{props.renderNotify()}</small> */}
      <textarea></textarea>
      <br />
      <br />
      <button className="btn btn-success">Gửi</button>
    </div>
  );
}

// Giúp các component này không re-render lại nhưng vẫn giữ dc bản chất của component
export default memo(ChildUseCallBack)
