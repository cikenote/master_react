import React, { useRef, useState } from "react";

export default function DemoUseRef(props) {
    let inputUserName = useRef(null);
    let inputpassword = useRef(null);
    let [userLogin, setUserLogin] = useState({userName: ""});

    let userName = "";

    const handleLogin = () => {
        //  console.log("ComInpitUsserName", inputUserName.current); // current là lấy ra giá trị của nó
        //  console.log("ComInpitUsserName", inputUserName.current);
        console.log(userName); 
        userName = "abv"
        setUserLogin({
            userName: userName
        })
    }
  return (
    <div className="container">
      <h3>Login</h3>
      <div className="form-group">
        <h4>UserName</h4>
        <input ref={inputUserName} name="userName" className="form-control" />
      </div>
      <div className="form-group">
        <h4>Password</h4>
        <input ref={inputpassword} name="userName" className="form-control" />
      </div>
      <div className="form-group">
        <button className="btn btn-success" onClick={() => {handleLogin()}} >Login</button>
      </div>
    </div>
  );
}


// dùng dome đến component
// dùng lưu giá trị để sau setSate giá trị vẫn giữ nguyên