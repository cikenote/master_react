import React, { useState, useEffect } from "react";
import ChildUseEffect from "./ChildUseEffect";

export default function DemoHookUseEffect() {
  // useEffect cho phép thực hiện side Effect (thay đổi các state ngoài hàm) bên trong các function Component
  // useEffect chạy sau khi DOM được cập nhật (sau render)
  // Vì vậy useEffect có thể ứng với 3 lifecycle chạy sau render đó là didMount, didUpdate, willUnmount

  let [number, setNumber] = useState(1);
  let [like, setLike] = useState(1);
  console.log(like);

  // Cách viết thay thế cho component didMount
  // setState sẽ không chạy lại disMount
  useEffect(() => {
    console.log("didMount");
  }, []);

  // Cách viết thay thế component didUpdatex
  useEffect(() => {
    console.log("disUpdate khi number thay đổi");
  }, [number]); // muốn biến nào thay đổi thì thêm ở đây nè
  // number là giá trị thay đổi sau render thì useEffect này sẽ chạy mà không chạy didMount
  // Number không thay đổi thì disUpdate sẽ không được chạy

  // => ta có state like ở ví dụ trên. Trạng thái của like thay đổi nhưng của number thì không
  // nên không hiện disUpdate. Chỉ khi number thay đổi thì mới hiện disUpdate còn không thì thôi

  // useEffect là hàm chạy sau khi giao diện render thay do didUpdate và didMount trong mọi trường hợp
  // Hàm effect chạy sau khi giao diện render
  // Viết như vậy thì sau render sẽ chạy disMount hoặc disUpdate trong mọi trường hợp
  // useEffect(() => {
  //   console.log("didMount");
  //   console.log("disUpdate");
  // })

  // khi chạy sẽ thấy render sẽ ra trước disMount và disUpdate trong useEffect
  console.log("render");

  const handleIncrease = () => {
    setNumber(number + 1);
  };

  return (
    <div className="m-5">
      <button
        onClick={() => {
          setLike(like + 1);
        }}
      >
        Like
      </button>
      <div className="card text-left" style={{ height: 300, width: 200 }}>
        <img
          style={{ height: 200, width: 200 }}
          className="card-img-top"
          src="https://i.pinimg.com/736x/a0/93/5c/a0935c2aa3c5e6097dc60f84267bae15.jpg"
          alt="Ảnh nè"
        />
        <div className="card-body">
          <h4 className="card-title text-danger">{number} ❤</h4>
          {/* <p className="card-text">Body</p> */}
        </div>
      </div>
      <button className="btn btn-success" onClick={handleIncrease}>
        +
      </button>

      {/* // component mất đi sẽ chạy useEffect */}
      {like === 1 ? <ChildUseEffect /> : ""}
    </div>
  );
}
