import React, { useReducer } from "react";

const initialCart = [];

const cartReducer = (state, action) => {
  switch (action.type) {
    case "addToCart": {
      let cartUpdate = [...state];
      let index = cartUpdate.findIndex(
        (itemCart) => itemCart.id === action.item.id
      );
      if (index !== -1) {
        cartUpdate[index].quantity++;
      } else {
        const itemCart = { ...action.item };
        cartUpdate.push(itemCart);
      }
      return cartUpdate;
    }
  }
  return [...state];
};

let arrProduct = [
  { id: "1", name: "Iphone", price: 1000, quantity: 1 },
  { id: "2", name: "Note 10plus", price: 5000, quantity: 1 },
  { id: "3", name: "Huawei P20", price: 12000, quantity: 1 },
  { id: "4", name: "Iphone XLS", price: 30000, quantity: 1 },
];

export default function DemoUseReducer(props) {
  let [cart, dispatch] = useReducer(cartReducer, initialCart);
  const addToCard = (itemClick) => {
    // console.log(itemClick)
    const action = {
      type: "addToCart",
      item: itemClick,
    };
    dispatch(action);
  };
  return (
    <div className="container">
      <div className="row">
        {arrProduct.map((item, index) => {
          return (
            <div key={index} className="col-3">
              <div className="card text-left">
                <img
                  className="card-img-top"
                  src={"https://picsum.photos/200/200"}
                  alt={index}
                />
                <div className="card-body">
                  <h4 className="card-title">{item.name}</h4>
                  <p className="card-text">{item.price}</p>
                  <button
                    className="btn btn-success"
                    onClick={() => {
                      addToCard(item);
                    }}
                  >
                    Ađ to card
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <h3>Giỏ hàng</h3>
      <table className="table">
        <thead>
          <tr>
            <th>id</th>
            <th>name</th>
            <th>price</th>
            <th>quantity</th>
            <th>total</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {cart.map((product, index) => {
            return (
              <tr key={index}>
                <td>{product.id}</td>
                <td>{product.name}</td>
                <td>{product.price}</td>
                <td>{product.quantity}</td>
                <td>{product.quantity * product.price}</td>
                <td>
                  <button className="btn btn-danger">x</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
