import React, { useEffect, useState } from "react";

export default function ChildUseEffect() {
  let [number, setNumber] = useState(1);

  console.log("render ChildUseEffect");

  // túm lại cách viết như vậy là vừa viết didMount vừa viết didUpdate
  useEffect(() => {
    // Viết cho disUpdate
    console.log("disUpdate chạy ở đây nè");
    return () => {
      // Xóa đi những gì không chạy, không xử dụng
      // return này không tự chạy, chỉ khi component mất đi thì mới kích hoạt thôi
      console.log("wilUnmount!!!");
    };
  }, [number]); // tham số thay đổi disUpdate sẽ lạy
  return (
    <div>
      <textarea></textarea> <br /> <br />
      <button className="btn btn-success">Gửi</button>
    </div>
  );
}
