import React, { useState } from "react";

export default function DemoHookUseState(props) {
    // useState nhận vào input là stateDefaults
    // Kết quả trả về là 1 tuple 2 tham số là state và hàm setState tương tự trong react LifeCycle (ReactClassComponent)
    // Trong 1 component có thể khai báo nhiều hàm useState
    // useState dùng để thay thế thuộc tính this.state tương dương react class component
  let [state, setState] = useState({ like: 0 });

  const handleLike = () => {
    // Lấy like tăng lên 1 và setState
    setState({
      like: state.like + 1,
    });
  };

  return (
    <div className="container m-5">
      <div className="card text-left" style={{ height: 250, width: 250 }}>
        <img
          className="card-img-top"
          src="https://phunugioi.com/wp-content/uploads/2022/02/anh-chan-dung-co-gai-trong-anh-chieu-ta.jpg"
          alt="ảnh nè"
        />
        <div className="card-body">
          <h4 className="card-title">Picture</h4>
          <p style={{ color: "red" }} className="card-text">
            {state.like} ❤
          </p>
        </div>
      </div>
      <button style={{margin: 250}} className="btn btn-danger" onClick={handleLike}>
        Like 
      </button>
    </div>
  );
}
