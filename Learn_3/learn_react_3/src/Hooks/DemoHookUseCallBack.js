import React, { useState, useCallback } from "react";
import ChildUseCallBack from "./ChildUseCallBack";

export default function DemoHookUseCallBack() {
  let [like, setLike] = useState(1);


  // khi setLike (like thay đổi) component sẽ render lại kể cả component ChildUseCallBack => ảnh hưởng đến performent

  const renderNotify = () => {
    return `Bạn đã thả ${like} ❤ !`
  }


  // Nếu bỏ [like] vào tham số thứ 2, thì khi like thay đổi thì sẽ render lại
  // Để [] vào tham số thứ 2, thì sẽ không bao giờ render lại
  const callBackRenderNotify = useCallback(renderNotify, [like]);

  return (
    <div className="container mt-5">
      Like: {like} ❤
      <br />
      <span
        style={{ cursor: "pointer", color: "red", fontSize: 35 }}
        onClick={() => {
          setLike(like + 1);
        }}
      >
        ❤
      </span>
      {/* Không có render, để đây thì không có gì để nói nên chuyển
        qua component ChilUseCallBack thì sao
      */}
      <small>{renderNotify()}</small>
      <br />
      <br />
      {/* Khi sử dụng memo ở component con nó sẽ không render lại nữa */}
      <ChildUseCallBack renderNotify={callBackRenderNotify}/>
    </div>
  );
}


// useCalBack dùng cho tình huống truyền props mà là một hàm, bận muốn render hay không render lại thì......