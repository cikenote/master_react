import React, { Component } from "react";
import "./UserProfile.css";
import Swal from 'sweetalert2'

export default class UserProfile extends Component {
  state = {
    values: {
      firstName: "",
      lastName: "",
      userName: "",
      email: "",
      password: "",
      passwordConfirm: "",
    },
    errors: {
      firstName: "",
      lastName: "",
      userName: "",
      email: "",
      password: "",
      passwordConfirm: "",
    },
  };

  handleChangeValue = (event) => {
    let { name, value, type } = event.target;
    // console.log(name, value);
    let newValue = { ...this.state.values, [name]: value };
    let newErrors = { ...this.state.errors };

    if (value.trim() === "") {
      newErrors[name] = name + " is required!";
    } else {
      newErrors[name] = "";
    }

    if (type === "email") {
      const regexEmail =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
      if (!regexEmail.test(value)) {
        // Nếu email không hợp lệ
        newErrors[name] = name + " is invalid!";
      } else {
        newErrors[name] = "";
      }
    }

    if (name === "passwordConfirm") {
      if (value === newValue["password"]) {
        newErrors[name] = "";
      } else {
        newErrors[name] = name + " is invalid!";
      }
    }

    this.setState({
      values: newValue,
      errors: newErrors,
    });

    // this.setState({
    //   values: { ...this.state.values, [name]: value }, // sinh ra một object mới với các thuộc tính cũ. [name]: value: đè lên thuộc tính cũ, nếu trùng chứ không sinh ra thuộc tính mới cùng name
    // });
    // Vì state nó bất đồng bộ nên nó đồng thời thực hiện 2 setState.
    // Khi bạn change 1 ô input nó sẽ binding ra luôn cái chữ lỗi khi chúng ta chưa nhập xong input => điều không mong muốn
    // this.setState({
    //   errors:{...this.state.errors, [name]:"Lỗi"},
    // })

    // if (value === "") {
    //   this.setState({
    //     errors: { ...this.state.errors, [name]: "Không được bỏ trống!" },
    //   });
    // } else {
    //   this.setState({
    //     errors: { ...this.state.errors, [name]: "" },
    //   });
    // }
  };

  handleSubmit = (event) => {
    // cản trình duyệt reload lại trang khi submit
    event.preventDefault();
    // xét điều kiện khi submit
    let { values, errors } = this.state;
    let valid = true; // Biến xác định form hợp lệ
    let profileContent = "";

    for (let key in values) {
      if (values[key] === "") {
        valid = false;
      }
    }
    for (let key in errors) {
      if (errors[key] !== "") {
        valid = false;
      }
    }

    if (!valid) {
      Swal.fire({
        title: 'Your profile invalid!',
        // html: profileContent,
        icon: 'error',
        confirmButtonText: 'Error'
      })
      return;
    }
    Swal.fire({
      title: 'Your profile success!',
      // html: profileContent,
      icon: 'success',
      confirmButtonText: 'Ok'
    })
  };

  render() {
    return (
      <div
        className="container-fluid"
        style={{
          backgroundColor: "#EEEEEE",
          position: "relative",
          height: "100vh",
          width: "100%",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <form
          onSubmit={this.handleSubmit}
          style={{
            fontSize:
              'font-family: "Google Sans", "Noto Sans Myanmar UI", arial, sans-serif',
            width: 600,
          }}
          className="container w-50 bg-white p-5 m-5"
        >
          <h1 className="text-center mt-0">User Profile</h1>
          <div className="mt-5">
            <div className="row">
              <div className="col-6">
                <div className="group">
                  <input
                    value={this.state.values.firstName}
                    type="text"
                    name="firstName"
                    required
                    onChange={this.handleChangeValue}
                  />
                  <span className="highlight" />
                  <span className="bar" />
                  <label>First Name</label>
                  <span className="text text-danger">
                    {this.state.errors.firstName}
                  </span>
                </div>
              </div>
              <div className="col-6">
                <div className="group">
                  <input
                    value={this.state.values.lastName}
                    type="text"
                    name="lastName"
                    required
                    onChange={this.handleChangeValue}
                  />
                  <span className="highlight" />
                  <span className="bar" />
                  <label>Last Name</label>
                  <span className="text text-danger">
                    {this.state.errors.lastName}
                  </span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="group">
                  <input
                    value={this.state.values.userName}
                    type="text"
                    name="userName"
                    required
                    onChange={this.handleChangeValue}
                  />
                  <span className="highlight" />
                  <span className="bar" />
                  <label>User Name</label>
                  <span className="text text-danger">
                    {this.state.errors.userName}
                  </span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="group">
                  <input
                    value={this.state.values.email}
                    type="email"
                    typeof="email"
                    name="email"
                    required
                    onChange={this.handleChangeValue}
                  />
                  <span className="highlight" />
                  <span className="bar" />
                  <label>Email Name</label>
                  <span className="text text-danger">
                    {this.state.errors.email}
                  </span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <div className="group">
                  <input
                    value={this.state.values.password}
                    type="password"
                    name="password"
                    required
                    onChange={this.handleChangeValue}
                  />
                  <span className="highlight" />
                  <span className="bar" />
                  <label>Password</label>
                  <span className="text text-danger">
                    {this.state.errors.password}
                  </span>
                </div>
              </div>
              <div className="col-6">
                <div className="group">
                  <input
                    value={this.state.values.passwordConfirm}
                    type="password"
                    name="passwordConfirm"
                    required
                    onChange={this.handleChangeValue}
                  />
                  <span className="highlight" />
                  <span className="bar" />
                  <label>Password Confirm</label>
                  <span className="text text-danger">
                    {this.state.errors.passwordConfirm}
                  </span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <button className="btn text-white bg-dark w-100">
                  Submit Form
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
