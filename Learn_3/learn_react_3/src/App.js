import './App.css';
import DemoHookUseCallBack from './Hooks/DemoHookUseCallBack';
import DemoHookUseEffect from './Hooks/DemoHookUseEffect';
import DemoHookUseMemo from './Hooks/DemoHookUseMemo';
// import UserProfile from './FormValidation/UserProfile/UserProfile';
// import LifeCycleReact from './LifeCycleReact/LifeCycleReact';
import DemoHookUseState from './Hooks/DemoHookUseState';
import DemoUseReducer from './Hooks/DemoUseReducer';
import DemoUseRef from './Hooks/DemoUseRef';

function App() {
  return (
    <div className="App">
      {/* <UserProfile/> */}
      {/* <LifeCycleReact/> */}
      {/* <DemoHookUseState/> */}
      {/* <DemoHookUseEffect/> */}
      {/* <DemoHookUseCallBack/> */}
      {/* <DemoHookUseMemo/> */}
      {/* <DemoUseRef/> */}
      <DemoUseReducer/>
    </div>
  );
}

export default App;
