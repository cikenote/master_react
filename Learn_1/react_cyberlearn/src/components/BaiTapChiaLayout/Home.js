import React, { Component } from "react";
import HeaderDemo from "./HeaderDemo";
import Navigation from "./Navigation";
import Content from "./Content";
import Footer from "./Footer";

// eslint-disable-next-line import/no-anonymous-default-export
export default class extends Component {
  render() {
    let style = {
      padding: 0,
    }
    return (
      <div className="container-fluid">
        <div style={style} className="row">
          <div className="col-12">
            <HeaderDemo />
          </div>
        </div>
        <div className="row">
          <div style={style} className="col-4 h-50">
            <Navigation/>
          </div>
          <div style={style} className="col-8">
            <Content/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
          <Footer/>
          </div>
        </div>
      </div>
    );
  }
}
