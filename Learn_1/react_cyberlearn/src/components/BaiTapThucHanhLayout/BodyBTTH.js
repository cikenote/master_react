import React, { Component } from "react";
import BanerBTTH from "./BanerBTTH";
import ItemBTTH from "./ItemBTTH";

export default class BodyBTTH extends Component {
  render() {
    return (
      <div className="container">
        <BanerBTTH />
        <div className="row">
          <div className="col-3">
            <ItemBTTH />
          </div>
          <div className="col-3">
            <ItemBTTH />
          </div>{" "}
          <div className="col-3">
            <ItemBTTH />
          </div>{" "}
          <div className="col-3">
            <ItemBTTH />
          </div>
        </div>
      </div>
    );
  }
}
