import React, { Component } from "react";

export default class StateDemo extends Component {
  // State là thuộc tính có sẵn cửa react class component giúp định nghĩa những nội dung có khả năng thay đổi khi người dùng thao tác sự kiện

  state = {
    status: false,
  };

  userLogin = {
    name: "Cikenote",
    age: 20,
  };


  // setState là phương thức có sẵn của react class component giúp thay đổi giá trị state và render lại giao điện
  login = () => {
    // this.state.status = true; // KHông được set giá trị trực tiếp trên state mà phải thông qua phương thức setState

    let newState = {
        status: true,
    }
    // Gọi phương thức setState => truyền vào State mới
    // setState là phương thức bất đồng bộ
    // setState có 2 tham số
    // this.setState(tham số 1, tham số 2);
    // - Tham số 1: là giá trị state mới
    // - Tham số 2: là callback thực thi ngay sau khi state thay đổi
    this.setState(newState, () => {
        console.log(this.state);
    });


    // log ra thì state vẫn là false vì nó bất đồng bộ
    // console.log(this.state)

    // Dùng cách dưới cho gọn cũng được
    // this.setState({ status: true });
  };

  renderUserLogin = () => {
    if (this.state.status === true) {
      return (
        <div className="text-center">UserLogin: {this.userLogin.name}</div>
      );
    }
    return (
      <button
        onClick={() => {
          this.login();
        }}
        className="text-center btn btn-success"
      >
        Đăng nhập
      </button>
    );
  };

  render() {
    return <div>{this.renderUserLogin()}</div>;
  }
}
