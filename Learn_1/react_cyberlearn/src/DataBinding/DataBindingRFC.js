import React from "react";

const school = "FPTU-HCM";
const student1 = {
  name: "Danh tiên xinh1",
  age: 20,
};

const DataBindingRFC = () => {
  const name = "Cikenote";

  const renderInfoVirus = () => {
    const virus = {
      id: "Covid-19",
      name: "Corona",
      alias: "SARs-CoV-2",
      img: "https://images.unsplash.com/photo-1605289982774-9a6fef564df8?q=80&w=1064&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    };
    return (
      <div className="card text-white bg-primary w-50 h-50">
        <img className="card-img-top" src={virus.img} alt={virus.id} />
        <div>
          <h4>{virus.name}</h4>
          <p>{virus.alias}</p>
        </div>
      </div>
    );
  };


  return (
    <div>
      <h1>React Functional DataBinding</h1>
      <hr />
      <h1>Hello, {name}</h1>
      <h1 className="text-danger">
        Hello, {student1.name} - Age: {student1.age} - School: {school}
      </h1>
      {renderInfoVirus()}
    </div>
  );
};

export default DataBindingRFC;
