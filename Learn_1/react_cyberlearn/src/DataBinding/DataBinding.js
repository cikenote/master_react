import React, { Component } from "react";
import HeaderDemo from "../components/BaiTapChiaLayout/HeaderDemo";

export default class DataBinding extends Component {
  // Tạo 1 thuộc tính
  name = "Cikenote";

  // Tạo 1 đối tượng với các thuộc tính
  student = {
    name: "Danh tiên xinh",
    age: 20,
  };

  // Binding phương thức (Hàm của lớp đối tượng)
  // Tát cả hàm render đều phải trả về Component (jsx)
  renderImage = () => {
    return (
      <img
        src="https://images.unsplash.com/photo-1616101746997-66ec55fa004f?q=80&w=1171&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
        alt="Ảnh nè"
      />
    );
  };

  renderMultiComponent = () => {
    return <HeaderDemo />;
  };

  renderInfoVirus = () => {
    const virus = {
        id: "Covid-19",
        name: "Corona",
        alias: "SARs-CoV-2",
        img: "https://images.unsplash.com/photo-1605289982774-9a6fef564df8?q=80&w=1064&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
      };
      return (
        <div className="card text-white bg-primary w-50 h-50">
            <img className="card-img-top" src={virus.img} alt={virus.id}/>
            <div>
                <h4>{virus.name}</h4>
                <p>{virus.alias}</p>
            </div>
        </div>
      )
  };

  render() {
    // Biến của hàm render => không sử dụng được cho hàm khác
    const school = "FPTU-HCM";
    const student1 = {
      name: "Danh tiên xinh1",
      age: 20,
    };
    return (
      <div>
        <h1>React Class Component DataBinding</h1>
        <h1>
          Hello, {this.name} - School: {school}
        </h1>
        <h1>Binding Object</h1>
        <h3 id="title">
          Name: {this.student.name} - Age: {this.student.age}
        </h3>
        <h3 id="title">
          Name: {student1.name} - Age: {student1.age}
        </h3>
        <h3>Binding function</h3>
        {this.renderImage()}
        {this.renderMultiComponent()}
        {this.renderInfoVirus()}
      </div>
    );
  }
}
