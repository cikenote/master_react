import React, { Component } from "react";
import "./Styling.css";
import Child from "./Child";
import style from "./Styling.module.css";
export default class Styling extends Component {
  render() {
    // Lưu ý: Viết dưới dạng thuộc tính document.getElementById("id").style
    let styleText = {
      color: "pink",
      padding: "15px",
      backgroundColor: "black",
    };

    return (
      <div>
        <Child />
        <p className="txt">Styling</p>
        <p className={style.txtStyle}>Hello Danh tiên xinh</p>
        <h3 style={styleText}>Danh đệp trai nè</h3>
        <h3
          style={{
            color: "blue",
            padding: "15px",
            backgroundColor: "black",
          }}
        >
          Danh đệp trai nè
        </h3>
      </div>
    );
  }
}
