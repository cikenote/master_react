import "./App.css";
import BaiTapChonXe from "./BaiTapChonXe/BaiTapChonXe";
import BaiTapRenderFilms from "./BaiTapRenderFilms/BaiTapRenderFilms";
import HandleEvent from "./HandleEvent/HandleEvent";
import RenderWithLoop from "./RenderWithLoop/RenderWithLoop";
import RenderingCondition from "./RenderingConditions/RenderingCondition";
import StateDemo from "./State/StateDemo";
import Styling from "./Styling/Styling";

function App() {
  return (
    <div className="App">
      {/* <HandleEvent /> */}
      {/* <RenderingCondition/> */}
      {/* <StateDemo/> */}
      {/* <Styling/>
      <p className="txt">App Component nè</p> */}
      {/* <BaiTapChonXe/> */}

      {/* <RenderWithLoop/> */}
      <BaiTapRenderFilms/>
    </div>
  );
}

export default App;
