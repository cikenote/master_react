import React, { Component } from "react";

export default class HandleEvent extends Component {

    // Định nghĩa hàm xử lý sự kiện khi người dùng click vào button click me
    handlClick = (name) => {
        alert("Hello " + name)
    }

    // Truyền tham số xử lý khi click
    handlClickParam = (param, button) => {
        console.log("param ",param);
        console.log("param ",param);

        console.log("button: ", button)
    }

  render() {
    return (
      <div>
        <button
          id="btnClickMe"
          onClick={() => {
            this.handlClick("Cikenote")
          }}
        >
          Click Me!
        </button>
        <button id="btnClickMe1" onClick={this.handlClickParam.bind(this, "Hello men!")}>Click Me!</button>
      </div>
    );
  }
}
