import React, { Component } from "react";

export default class RenderingCondition extends Component {
  login = true;
  userName = "Cikenote";

  renderContent = () => {
    if (this.login) {
      return <p>Hello {this.userName}</p>;
    }
    <button>Đăng nhập</button>;
  };

  render() {
    return <div>{this.renderContent()}</div>;
  }
}
