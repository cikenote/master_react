import React, { Component } from "react";

export default class RenderWithLoop extends Component {
  productList = [
    {
      id: 1,
      name: "black car",
      price: 1000,
      img: "./assets/products/black-car.jpg",
    },
    {
      id: 2,
      name: "steel car",
      price: 1000,
      img: "./assets/products/steel-car.jpg",
    },
    {
      id: 3,
      name: "silver car",
      price: 1000,
      img: "./assets/products/silver-car.jpg",
    },
    {
      id: 4,
      name: "red car",
      price: 1000,
      img: "./assets/products/red-car.jpg",
    },
  ];

  renderTable = () => {
    // Cách 1: Render array sử dụng for
    // let mangTrComponent = [];
    // for (let index = 0; index < this.productList.length; index++) {
    //   let product = this.productList[index];
    //   let trJSX = (
    //     <tr key={index}>
    //       <td>{product.id}</td>
    //       <td>{product.name}</td>
    //       <td>{product.price}</td>
    //       <td>
    //         <img style={{ width: "100px" }} src={product.img} />
    //       </td>
    //     </tr>
    //   );

    // Cách 2: Dùng hàm map có sẵn của ES6;
    let mangTrComponent = this.productList.map((product, index) => {
      return (
        <tr key={index}>
          <td>{product.id}</td>
          <td>{product.name}</td>
          <td>{product.price}</td>
          <td>
            <img style={{ width: "200px" }} src={product.img} alt={product.id}/>
          </td>
        </tr>
      );
    });
    return mangTrComponent;
  };

  render() {
    return (
      <div classname="container">
        <table className="table">
          <thead>
            <tr>
              <th>id</th>
              <th>name</th>
              <th>price</th>
              <th>img</th>
            </tr>
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}
